== RESTful API ==

RESTful API is available at `[base URL]/api`.

=== Media types ===

This application supports _application/json_ media type for all resources.

=== Resource representation in JSON ===

Each entity must be represented by an object that must have an _href_ property.
The _href_ property must contain the URL of the entity resource.

Each collection must be represented by an object containing the following
properties:

* _href_ property must contain the URL of the resource;
* _items_ property must be a list of objects representing entities in the
  collection (or requested part of the collection).

Representations of partial collections should also contain the following
properties:

* _offset_ optional property should contain the index of the first entity in the
collection returned by the server if only a part of the collection was
requested;
* _limit_ optional property should contain the number of entities in the
collection returned by the server if only a part of the collection was
requested;
* `"first": {"href": "[collection]?offset=0&limit=[limit]"}`
* `"previous": {"href": "[collection]?offset=[offset]&limit=[limit]"}` or
`"previous": null`
* `"next": {"href": "[collection]?offset=[offset]&limit=[limit]"}` or
`"next": null`
* `"last": {"href": "[collection]?offset=[offset]&limit=[limit]"}`
* _count_ or _size_?

*TODO: Entity expansion.*

Nested entities and collections not owned by the requested resource should not
be populated if their inclusion may put additional load on the server or create
recursion. Instead, they will be represented by objects containing only
_href_ property specifying the URL where the client may request the complete
content of the referenced object of collection.

    {
        "href": "[base URL]/api/users/johndoe"
        "name": "John Doe",
        ...
        "groups": {
            "href": "[base URL]/api/groups?user=johndoe"
            // or "[base URL]/api/users/johndoe/groups"??
        }
    }

Client may request population (aka expansion) of nested entities by specifying
the _expand_ parameter in the resource URL. The _expand_ parameter should
contain a comma-separated list of fields that contain objects (or lists of
objects) that should be populated in the response. For example, resource
`[base URL]/api/users/johndoe?expand=groups` should return a response where
_groups_ property contains the populated object representing the collection of
groups assigned to the user.

Note that items in collections also should not be expanded by default:

    {
        "href": "[collection]",
        "items": [
            {
                "href": [item 1]
            },
            {
                "href": [item 2]
            },
            ...
        ]
    }

*TODO: Partial entities.*

`[base URL]/api/users/johndoe?fields=name,email,groups(name)`

*TODO: Limit and offset*

*TODO: Errors*

    {
        "status": 400,
        "code": 123456,
        "property": "name",
        "massage": "User with that name already exists.",
        "developerMessage": "", //details?
        "moreInfo": "[link to documentation]"
    }

=== Security ===

API should be kept stateless. Every request requiring authorization must be
authenticated individually. Authorization decisions should be made based on
resource content.

=== Caching ===

*TODO: ETags*

=== Resources ===

* `/api/organizations`

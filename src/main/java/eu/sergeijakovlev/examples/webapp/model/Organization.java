package eu.sergeijakovlev.examples.webapp.model;

public interface Organization {
    Long getId();
    String getName();
}

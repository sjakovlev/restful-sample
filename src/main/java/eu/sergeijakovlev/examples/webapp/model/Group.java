package eu.sergeijakovlev.examples.webapp.model;

public interface Group {
    Long getId();
    String getName();
}

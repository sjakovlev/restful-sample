package eu.sergeijakovlev.examples.webapp.model;

public interface User {
    Long getId();
    String getName();
    String getEmail();
}

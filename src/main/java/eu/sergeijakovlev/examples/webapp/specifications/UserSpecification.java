package eu.sergeijakovlev.examples.webapp.specifications;

public interface UserSpecification
        extends CollectionSpecification<UserSpecification.SortField> {
    String getNamePattern();
    String getEmailPattern();
    enum SortField {
        NAME,
        EMAIL
    }
}

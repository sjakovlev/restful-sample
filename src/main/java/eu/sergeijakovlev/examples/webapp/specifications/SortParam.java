package eu.sergeijakovlev.examples.webapp.specifications;

public interface SortParam<T> {
    T getParam();
    Order getOrder();

    enum Order {
        ASC,
        NONE,
        DESC
    }
}
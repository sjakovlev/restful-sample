package eu.sergeijakovlev.examples.webapp.specifications;

public interface GroupSpecification
        extends CollectionSpecification<GroupSpecification.SortField> {
    String getNamePattern();
    enum SortField {
        NAME
    }
}

package eu.sergeijakovlev.examples.webapp.specifications;

public interface OrganizationSpecification
        extends CollectionSpecification<OrganizationSpecification.SortField> {
    String getNamePattern();
    enum SortField {
        ID,
        NAME
    }
}

package eu.sergeijakovlev.examples.webapp.specifications;

import java.util.List;

public interface CollectionSpecification<S> {
    List<SortParam<S>> getSortParams();
    Integer getOffset();
    Integer getLimit();
}

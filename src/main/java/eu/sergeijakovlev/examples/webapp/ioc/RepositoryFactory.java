package eu.sergeijakovlev.examples.webapp.ioc;

import org.glassfish.hk2.api.Factory;

public class RepositoryFactory<T> implements Factory<T> {
    private final Class<? extends T> clazz;

    public RepositoryFactory(Class<? extends T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T provide() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Could not initialize repository", e);
        }
    }

    @Override
    public void dispose(T instance) {

    }
}

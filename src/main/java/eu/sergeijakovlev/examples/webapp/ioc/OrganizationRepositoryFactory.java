package eu.sergeijakovlev.examples.webapp.ioc;

import eu.sergeijakovlev.examples.webapp.persistence.db.jpa.OrganizationRepositoryImpl;
import eu.sergeijakovlev.examples.webapp.repositories.OrganizationRepository;

public class OrganizationRepositoryFactory
        extends RepositoryFactory<OrganizationRepository> {
    public OrganizationRepositoryFactory() {
        super(OrganizationRepositoryImpl.class);
    }
}

package eu.sergeijakovlev.examples.webapp;

import eu.sergeijakovlev.examples.webapp.ioc.OrganizationRepositoryFactory;
import eu.sergeijakovlev.examples.webapp.repositories.OrganizationRepository;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("api")
class WebappResourceConfig extends ResourceConfig {
    WebappResourceConfig() {
        packages("eu.sergeijakovlev.examples.webapp.resources");
        register(WebappExceptionMapper.class);

        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(OrganizationRepositoryFactory.class)
                        .to(OrganizationRepository.class)
                        .proxy(true).proxyForSameScope(false)
                        .in(RequestScoped.class);
            }
        });
    }
}

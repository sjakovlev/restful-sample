package eu.sergeijakovlev.examples.webapp.repositories;

import eu.sergeijakovlev.examples.webapp.model.Organization;
import eu.sergeijakovlev.examples.webapp.specifications.OrganizationSpecification;

public interface OrganizationRepository
        extends CollectionRepository<Organization, OrganizationSpecification> {
}

package eu.sergeijakovlev.examples.webapp.repositories;

import eu.sergeijakovlev.examples.webapp.model.User;
import eu.sergeijakovlev.examples.webapp.specifications.UserSpecification;

public interface UserRepository
        extends CollectionRepository<User, UserSpecification> {
}

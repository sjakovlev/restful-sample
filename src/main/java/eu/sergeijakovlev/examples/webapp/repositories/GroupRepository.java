package eu.sergeijakovlev.examples.webapp.repositories;

import eu.sergeijakovlev.examples.webapp.model.Group;
import eu.sergeijakovlev.examples.webapp.specifications.GroupSpecification;

public interface GroupRepository
        extends CollectionRepository<Group, GroupSpecification>{
}

package eu.sergeijakovlev.examples.webapp.repositories;

import java.util.List;

public interface CollectionRepository<T, S> {
    List<? extends T> findAll();
    List<? extends T> findAllBySpec(S spec);
    int countBySpec(S spec);
}

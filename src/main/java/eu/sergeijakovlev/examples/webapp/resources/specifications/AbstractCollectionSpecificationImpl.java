package eu.sergeijakovlev.examples.webapp.resources.specifications;

import eu.sergeijakovlev.examples.webapp.specifications.CollectionSpecification;
import eu.sergeijakovlev.examples.webapp.specifications.SortParam;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AbstractCollectionSpecificationImpl<S>
        implements CollectionSpecification<S> {

    public AbstractCollectionSpecificationImpl(
            @QueryParam("offset") Integer offset,
            @QueryParam("limit") Integer limit
    ) {
        setOffset(offset);
        setLimit(limit);
    }

    @Getter
    private List<SortParam<S>> sortParams = new ArrayList<>();

    @Getter
    private Integer offset = 0;

    public void setOffset(Integer offset) {
        log.trace("setOffset({})", offset);
        if (offset != null && offset >= 0) {
            this.offset = offset;
        } else {
            log.debug("Invalid offset, defaulting to 0");
            this.offset = 0;
        }
    }

    @Getter
    private Integer limit = Integer.MAX_VALUE;

    public void setLimit(Integer limit) {
        log.trace("setLimit({})", limit);
        if (limit != null && limit >= 0) {
            this.limit = limit;
        } else {
            log.debug("Invalid limit, defaulting to MAX_VALUE");
            this.limit = Integer.MAX_VALUE;
        }
    }
}

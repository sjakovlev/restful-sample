package eu.sergeijakovlev.examples.webapp.resources.specifications;

import eu.sergeijakovlev.examples.webapp.specifications.OrganizationSpecification;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.QueryParam;
import java.util.List;

@Slf4j
public class OrganizationSpecificationImpl
        extends AbstractCollectionSpecificationImpl<OrganizationSpecification.SortField>
        implements OrganizationSpecification {

    @Getter
    @Setter
    private String namePattern;

    public OrganizationSpecificationImpl(
            @QueryParam("offset") Integer offset,
            @QueryParam("limit") Integer limit,
            @QueryParam("sort") List<OrganizationSortParam> sortParams,
            @QueryParam("name") String name) {
        super(offset, limit);
        this.namePattern = name;
        log.warn("Sort params: {}", sortParams);
    }

    public static class OrganizationSortParam extends SortParamImpl<SortField> {
        public OrganizationSortParam(String repr) {
            super(SortField.class, repr);
        }
    }
}

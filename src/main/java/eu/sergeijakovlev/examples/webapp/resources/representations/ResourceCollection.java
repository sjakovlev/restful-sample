package eu.sergeijakovlev.examples.webapp.resources.representations;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

public class ResourceCollection<T extends Resource>
        extends Resource {
    @Getter
    @Setter
    Collection<T> items;
}

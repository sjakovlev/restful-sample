package eu.sergeijakovlev.examples.webapp.resources.specifications;

import eu.sergeijakovlev.examples.webapp.specifications.SortParam;
import lombok.Getter;

public class SortParamImpl<T extends Enum<T>> implements SortParam<T> {
    private static final String INVERT_ORDER = "-";

    @Getter
    private T param;

    @Getter
    private Order order;

    public SortParamImpl(Class<T> clazz, String repr) {
        if(repr.startsWith(INVERT_ORDER)) {
            this.order = Order.DESC;
            this.param = Enum.valueOf(clazz, repr.substring(1));
        } else {
            this.order = Order.ASC;
            this.param = Enum.valueOf(clazz, repr);
        }
    }

    @Override
    public String toString() {
        return "SortParam{param="
                + param.toString() + ", order="
                + order.toString() + "}";
    }
}

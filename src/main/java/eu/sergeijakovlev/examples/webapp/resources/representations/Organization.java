package eu.sergeijakovlev.examples.webapp.resources.representations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Organization extends Resource {
    @Getter
    @Setter
    Long id;

    @Getter
    @Setter
    String name;
}

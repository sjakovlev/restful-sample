package eu.sergeijakovlev.examples.webapp.resources;

import eu.sergeijakovlev.examples.webapp.repositories.OrganizationRepository;
import eu.sergeijakovlev.examples.webapp.resources.representations.Organization;
import eu.sergeijakovlev.examples.webapp.resources.representations.ResourceCollection;
import eu.sergeijakovlev.examples.webapp.resources.specifications.OrganizationSpecificationImpl;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Path("/organizations")
@Produces(MediaType.APPLICATION_JSON)
public class OrganizationResource {

    private OrganizationRepository organizationRepository;

    @Inject
    public OrganizationResource(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Context
    UriInfo uriInfo;

    @GET
    public ResourceCollection<Organization> getOrganizations(
            @BeanParam OrganizationSpecificationImpl specification
    ) {
        List<? extends eu.sergeijakovlev.examples.webapp.model.Organization> all =
                organizationRepository.findAllBySpec(specification);

        List<Organization> ol = new ArrayList<>();
        for (eu.sergeijakovlev.examples.webapp.model.Organization org : all) {
            ol.add(new Organization(org.getId(), org.getName()));
        }

        ResourceCollection<Organization> c = new ResourceCollection<>();
        c.setItems(ol);
        c.setHref(uriInfo.getAbsolutePath());
        return c;
    }
}

package eu.sergeijakovlev.examples.webapp.resources.representations;

import lombok.Getter;
import lombok.Setter;

import java.net.URI;

public abstract class Resource {
    @Getter
    @Setter
    URI href;
}

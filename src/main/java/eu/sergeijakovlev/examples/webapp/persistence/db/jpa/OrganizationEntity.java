package eu.sergeijakovlev.examples.webapp.persistence.db.jpa;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import eu.sergeijakovlev.examples.webapp.model.Organization;
import lombok.Getter;
import lombok.Setter;

@Entity
class OrganizationEntity implements Organization, Serializable {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Getter
    @Setter
    private String name;
}

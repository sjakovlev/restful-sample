package eu.sergeijakovlev.examples.webapp.persistence.db.jpa;

import eu.sergeijakovlev.examples.webapp.model.Organization;
import eu.sergeijakovlev.examples.webapp.repositories.OrganizationRepository;
import eu.sergeijakovlev.examples.webapp.specifications.OrganizationSpecification;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.*;
import java.util.List;

public class OrganizationRepositoryImpl implements OrganizationRepository {
    EntityManager em = Persistence.createEntityManagerFactory("application")
            .createEntityManager();
    @Override
    public List<? extends Organization> findAll() {

        em.getTransaction().begin();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OrganizationEntity> cq =
                cb.createQuery(OrganizationEntity.class);
        List<OrganizationEntity> orgs = em.createQuery(cq).getResultList();
        em.getTransaction().commit();
        return orgs;
    }

    private Predicate toPredicate(OrganizationSpecification spec,
                                  Root<OrganizationEntity> root,
                                  CriteriaBuilder cb) {
        Predicate predicate = cb.and();
        String namePattern = spec.getNamePattern();
        if (namePattern != null) {
            Path<String> name = root.get(root.getModel()
                    .getSingularAttribute("name", String.class));
            cb.and(
                    predicate,
                    cb.like(
                            cb.lower(name),
                            namePattern.toLowerCase()
                    )
            );
        }
        return predicate;
    }

    @Override
    public List<? extends Organization> findAllBySpec(
            OrganizationSpecification spec) {
        em.getTransaction().begin();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OrganizationEntity> cq =
                cb.createQuery(OrganizationEntity.class);
        Root<OrganizationEntity> root = cq.from(OrganizationEntity.class);
        //cq.select(root);
        cq.where(toPredicate(spec, root, cb));

        List<OrganizationEntity> orgs = em.createQuery(cq)
                .setFirstResult(spec.getOffset())
                .setMaxResults(spec.getLimit())
                .getResultList();
        em.getTransaction().commit();
        return orgs;
    }

    @Override
    public int countBySpec(OrganizationSpecification spec) {
        return 0;
    }
}

package eu.sergeijakovlev.examples.webapp.persistence.db.jpa;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
class UserEntity implements Serializable {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    @ManyToOne
    private OrganizationEntity organization;
}

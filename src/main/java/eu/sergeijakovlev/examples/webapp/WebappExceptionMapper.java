package eu.sergeijakovlev.examples.webapp;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class WebappExceptionMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable exception) {
        log.warn("Unexpected exception", exception);
        return Response.status(500).build();
    }
}

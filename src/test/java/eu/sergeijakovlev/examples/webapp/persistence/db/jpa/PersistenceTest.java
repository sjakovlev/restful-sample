package eu.sergeijakovlev.examples.webapp.persistence.db.jpa;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PersistenceTest {
    private final static String PERSISTENCE_UNIT = "application";

    private final static Map<String, String> DB_PROPERTIES = createProperties();

    private static EntityManagerFactory entityManagerFactory;

    private static Map<String, String> createProperties() {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.url", "jdbc:derby:memory:testDb;create=true");
        properties.put("javax.persistence.schema-generation.database.action", "drop-and-create");
        return properties;
    }

    @BeforeClass
    public static void setUpClass () {
        entityManagerFactory = Persistence.createEntityManagerFactory(
                PERSISTENCE_UNIT, DB_PROPERTIES);
    }

    @Before
    public void setUp() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        OrganizationEntity org1 = new OrganizationEntity();
        org1.setName("John Doe & Co");

        UserEntity org1user1 = new UserEntity();
        org1user1.setName("John Doe");
        org1user1.setEmail("johndoe@doe.com");
        org1user1.setOrganization(org1);

        UserEntity org1user2 = new UserEntity();
        org1user2.setName("Jane Doe");
        org1user2.setEmail("janedoe@doe.com");
        org1user2.setOrganization(org1);

        UserEntity org1user3 = new UserEntity();
        org1user3.setName("Larry Smith");
        org1user3.setEmail("larrysmith@doe.com");
        org1user3.setOrganization(org1);

        GroupEntity org1group1 = new GroupEntity();
        org1group1.setName("Does");
        org1group1.setOrganization(org1);
        org1group1.getUsers().add(org1user1);
        org1group1.getUsers().add(org1user2);

        GroupEntity org1group2 = new GroupEntity();
        org1group2.setName("HR");
        org1group2.setOrganization(org1);
        org1group2.getUsers().add(org1user2);
        org1group2.getUsers().add(org1user3);

        entityManager.persist(org1);
        entityManager.persist(org1user1);
        entityManager.persist(org1user2);
        entityManager.persist(org1user3);
        entityManager.persist(org1group1);
        entityManager.persist(org1group2);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @After
    public void tearDown() {
        /*EntityManager entityManager =
                entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createQuery("DELETE FROM demo_group")
                .executeUpdate();
        entityManager.createQuery("DELETE FROM demo_user")
                .executeUpdate();
        entityManager.createQuery("DELETE FROM demo_organization")
                .executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();*/
    }

    @Test
    public void checkTestData() {
        EntityManager entityManager =
                entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        checkTestDbOrganizations(entityManager, cb);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private void checkTestDbOrganizations(EntityManager entityManager,
                                          CriteriaBuilder cb) {
        CriteriaQuery<OrganizationEntity> query = cb
                .createQuery(OrganizationEntity.class);
        Root<OrganizationEntity> root = query.from(OrganizationEntity.class);
        query.select(root);
        query.orderBy(cb.asc(root.get("name")));
        List resultList = entityManager
                .createQuery(query)
                .getResultList();
        assertEquals(1, resultList.size());
        OrganizationEntity org1 = (OrganizationEntity) resultList.get(0);
        assertEquals("John Doe & Co", org1.getName());
    }
}
